## Deploy the application

```
kubectl create -f https://bitbucket.org/leanitdevs/k8s-cluster-configs/raw/HEAD/k8s-zero-hero-cluster.yaml
```

## Check that the Cluster is running

```
kubectl get all -l app=dev-marche-demo
```

## Call the endpoint

```
curl -s http://`kubectl get service sender-svc -o yaml | grep ip | awk '{print $3}'`
```

To simulate multiple requests:

```
#!/bin/bash

ENDPOINT_URL=http://`kubectl get service sender-svc -o yaml | grep ip | awk '{print $3}'`

while true; do
    curl -s $ENDPOINT_URL
    sleep 0.01
done
```

## Scale the receivers

```
kubectl scale deployment receiver-deployment --replicas=5
```

## Deploy a specific version

```
kubectl set image deployment/receiver-deployment receiver=leanitrepo/ci-demo-receiver:100msec
```

## Undeploy the application

```
kubectl delete deployments,services -l app=dev-marche-demo
```
